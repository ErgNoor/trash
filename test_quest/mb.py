from ma import Second, Parent

class B(Second, Parent):
    def __init__(self, number):
        self.init_number = number
        self.isSecond = 1

    def fnc(self, f_number, s_number):
        return f_number * s_number * self.init_number

    def isFirst(self):
        return 0
    