class First(object):
    pass

class Second(object):
    pass

class Parent(object):
    pass

class MyError(Exception):
    pass

class A(First, Parent):
    def __init__(self):
        self.i = 3

    def fnc(self, number):
        if number == 7:
            raise MyError("Error text")

        return (number * number) * (number+1)

    def isFirst(self):
        return 1

    @property
    def isSecond(self):
        return 0